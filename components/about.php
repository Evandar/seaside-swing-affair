<!-- section start -->
<!-- ================ -->
<section id="about" class="light-gray-bg pv-30 clearfix">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h2 class="text-center"><?php pll_e('About'); ?></h2>
                <div class="separator"></div>
                <p>
                    <?php pll_e('If it wasn’t for the idea that two then-students of the Gdańsk University of Technology decided to make into reality, the people in Tricity would still not know the joys of dancing around to swing music. In April 2013, the first Lindy Hop workshops in Gdansk took place. Since then, thanks to the work of many awesome people, the community of Swing Revolution Trójmiasto keeps growing and attracting all sorts of new faces.') ?>
                    <br>
                </p>
                <p>
                    <?php pll_e('This coming year we are going to celebrate our <strong>fifth birthday</strong>: such occasion demands some huge celebration! We hope to share the birthday cake with dancers from all across Poland and Europe, tighten our Lindy bonds and dance till daybreak. See you in Gdansk!') ?>
                </p>
            </div>
        </div>
    </div>
</section>
<!-- section end -->