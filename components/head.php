<head>
    <meta charset="utf-8">
    <title>Seaside Swing Affair</title>
    <meta name="description" content="We are going to celebrate our fifth birthday: such occasion demands some huge celebration!">
    <meta name="author" content="Tomasz Miotk">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico">

    <!-- Web Fonts -->
    <link href='//fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
    <?php wp_head(); ?>

</head>