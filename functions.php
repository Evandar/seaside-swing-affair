<?php
/**
 * Seaside Swing Affair Template functions and definitions
 *
 * @package Seaside Swing Affair
 * @since Seaside Swing Affair Theme 1.0
 */

if ( ! function_exists( 'seaside_setup' ) ):
    function seaside_setup() {
        /**
         * Register main Menu
         */
        register_nav_menus( array(
            'header-menu' => __( 'Navbar Menu', 'Seaside' ),
        ) );
    }
endif; // shape_setup
add_action( 'after_setup_theme', 'seaside_setup' );

function load_css() {


    /* Bootstrap */
    wp_enqueue_style('Bootstrap css', get_template_directory_uri() . '/bootstrap/css/bootstrap.css');

    /* Font Awesome CSS */
    wp_enqueue_style('Font Awesome css', get_template_directory_uri() . '/fonts/font-awesome/css/font-awesome.css');

    /* Fontello CSS */
    wp_enqueue_style('Fontello  css', get_template_directory_uri() . '/fonts/fontello/css/fontello.css');

    /* Plugins */
    wp_enqueue_style('RS settings', get_template_directory_uri() . '/plugins/rs-plugin-5/css/settings.css');
    wp_enqueue_style('RS layers', get_template_directory_uri() . '/plugins/rs-plugin-5/css/layers.css');
    wp_enqueue_style('RS navigation', get_template_directory_uri() . '/plugins/rs-plugin-5/css/navigation.css');
    wp_enqueue_style('Animations', get_template_directory_uri() . '/css/animations.css');

    /* Main CSS files */
    wp_enqueue_style('Core CSS', get_template_directory_uri() . '/css/style.css');
    wp_enqueue_style('Typography CSS', get_template_directory_uri() . '/css/typography-default.css');
    wp_enqueue_style('Coror Scheme', get_template_directory_uri() . '/css/skins/dark_red.css');
    wp_enqueue_style('Flag CSS', get_template_directory_uri() . '/css/flag-icon.css');
    wp_enqueue_style('Custom CSS', get_template_directory_uri() . '/css/custom.css');

}
add_action( 'wp_enqueue_scripts' , 'load_css' );


function load_js() {
    wp_enqueue_script('jQuery' , 'https://code.jquery.com/jquery-2.2.4.min.js');
    wp_enqueue_script('Bootstrap JS' , 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js');
    wp_enqueue_script('Modernizer' , 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js');

    /* Revolution Slider */
    wp_enqueue_script('Tools' , get_template_directory_uri() . '/plugins/rs-plugin-5/js/jquery.themepunch.tools.min.js?rev=5.0');
    wp_enqueue_script('Revolution' , get_template_directory_uri() . '/plugins/rs-plugin-5/js/jquery.themepunch.revolution.min.js?rev=5.0');
    wp_enqueue_script('Revolution Anims' , get_template_directory_uri() . '/plugins/rs-plugin-5/js/extensions/revolution.extension.slideanims.min.js');
    wp_enqueue_script('Revolution Layers' , get_template_directory_uri() . '/plugins/rs-plugin-5/js/extensions/revolution.extension.layeranimation.min.js');
    wp_enqueue_script('Revolution Nav' , get_template_directory_uri() . '/plugins/rs-plugin-5/js/extensions/revolution.extension.navigation.min.js');
    wp_enqueue_script('Revolution Video' , get_template_directory_uri() . '/plugins/rs-plugin-5/js/extensions/revolution.extension.video.min.js');

    /* Apear JS */
    wp_enqueue_script('Waypoints' , 'https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.js');
    wp_enqueue_script('Sticky' , 'https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/shortcuts/sticky.min.js');

    /* Custom */
    wp_enqueue_script('Template JS' , get_template_directory_uri() . '/js/template.js');
    wp_enqueue_script('Custom JS' , get_template_directory_uri() . '/js/custom.js');


}
add_action( 'wp_enqueue_scripts' , 'load_js' );
add_theme_support( 'post-thumbnails');

/**
 * Turn off admin bar
 */
add_filter('show_admin_bar', '__return_false');

// Register Custom Navigation Walker
require_once('menu-nav-walker.php');



// ////////////////
// // Translations
// ////////////////
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

// Get url of page in selected language
function get_url_for_language( $original_url, $language )
{
    $post_id = url_to_postid( $original_url );
    $lang_post_id = icl_object_id( $post_id , 'page', true, $language );



    $url = "";
    if($lang_post_id != 0) {
        $url = get_permalink( $lang_post_id );
    }else {
        // No page found, it's most likely the homepage
        global $sitepress;
        $url = $sitepress->language_url( $language );
    }

    return $url;
}

/**
 * Disable the emoji's
 */
function disable_emojis() {
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
    add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 *
 * @param array $plugins
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
        return array_diff( $plugins, array( 'wpemoji' ) );
    } else {
        return array();
    }
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
    if ( 'dns-prefetch' == $relation_type ) {
        /** This filter is documented in wp-includes/formatting.php */
        $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

        $urls = array_diff( $urls, array( $emoji_svg_url ) );
    }

    return $urls;
}


/**
 *
 * Ascyn loading JS
 *
 */

function add_async_forscript($url)
{
    if (strpos($url, '#asyncload')===false)
        return $url;
    else if (is_admin())
        return str_replace('#asyncload', '', $url);
    else
        return str_replace('#asyncload', '', $url)."' async='async";
}
add_filter('clean_url', 'add_async_forscript', 11, 1);