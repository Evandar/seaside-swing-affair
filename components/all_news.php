<!-- section start -->
<!-- ================ -->
<section class="section tickets-bg ">
    <div class="container">
        <div class="col-lg-8 offset-lg-2">
            <h2 class="text-center"><?php pll_e('News'); ?></h2>
            <div class="separator"></div>
        </div>
        <div class="col-lg-12">
            <div class="row">
                <?php $custom_query = new WP_Query('posts_per_page=99'); // exclude category 9
                while($custom_query->have_posts()) : $custom_query->the_post(); ?>
                    <?php get_template_part( 'components/single_news', get_post_format() ); ?>
                <?php endwhile; ?>
                <?php wp_reset_postdata(); // reset the query ?>
            </div>
        </div>
    </div>
</section>
<!-- section end -->

