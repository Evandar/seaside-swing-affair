<!-- section start -->
<!-- ================ -->
<section class="light-gray-bg pv-30 clearfix">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 offset-lg-2">
                <h2 class="text-center"><?php the_title(); ?></h2>
                <div class="separator"></div>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post();
                    the_content();
                endwhile; else: ?>
                    <p>Sorry, no posts matched your criteria.</p>
                <?php endif; ?>
            </div>

        </div>
    </div>
</section>
<!-- section end -->