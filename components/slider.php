<!-- banner start -->
<!-- ================ -->
<div class="banner clearfix">

    <!-- slideshow start -->
    <!-- ================ -->
    <div class="slideshow">

        <!-- slider revolution start -->
        <!-- ================ -->
        <div class="slider-revolution-5-container">
            <div id="slider-banner-fullwidth-hero" class="slider-banner-fullscreen rev_slider" data-version="5.0">
                <ul class="slides">
                    <!-- slide 1 start -->
                    <!-- ================ -->
                    <li class="text-right" data-transition="random" data-slotamount="default" data-masterspeed="default" data-title="Next">

                        <!-- Transparent Background -->
                        <div class="tp-caption dark-translucent-bg"
                             data-x="center"
                             data-y="center"
                             data-start="0"
                             data-transform_in="opacity:0;s:600;e:Power2.easeInOut;"
                             data-transform_out="opacity:0;s:600;s:300;"
                             data-width="5000"
                             data-height="5000">
                        </div>

                        <!-- Video Background -->
                        <div class="rs-background-video-layer"
                             data-forcerewind="on"
                             data-volume="mute"
                             data-videowidth="100%"
                             data-videoheight="100%"
                             data-videomp4="<?php echo get_template_directory_uri(); ?>/videos/slider.mp4"
                             data-videopreload="preload"
                             data-videoloop="loop"
                             data-aspectratio="16:9"
                             data-autoplay="true"
                             data-autoplayonlyfirsttime="false"
                             data-nextslideatend="false">
                        </div>

                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption large-slider"
                             data-x="center"
                             data-y="150"
                             data-start="1000"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:1100;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:[100%];s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">
                            <span>Seaside <br>Swing <br>Affair</span>
                        </div>

                        <!-- LAYER NR. 2 -->
                        <div class="tp-caption large_white tp-resizeme"
                             data-x="center"
                             data-y="480"
                             data-start="750"
                             data-transform_idle="o:1;"
                             data-transform_in="o:0;s:2000;e:Power4.easeInOut;">
                            <div class="separator light"></div>
                        </div>
                        <!-- LAYER NR. 3 -->
                        <div class="tp-caption large_white"
                             data-x="center"
                             data-y="550"
                             data-start="750"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[100%];sX:1;sY:1;s:850;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;">
                             <span>
                                3-6/05/2018<br>
                             </span>
                        </div>
                        <div class="tp-caption large_white"
                             data-x="center"
                             data-y="600"
                             data-start="750"
                             data-transform_idle="o:1;"
                             data-transform_in="y:[100%];sX:1;sY:1;s:850;e:Power4.easeInOut;"
                             data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;"
                             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                             data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
                        >
                            <span>
                                Gdańsk | <?php pll_e('Poland'); ?><br>
                            </span>
                        </div>
                    </li>
                    <!-- slide 1 end -->
                </ul>
                <div class="tp-bannertimer"></div>
            </div>
        </div>
        <!-- slider revolution end -->
    </div>
    <!-- slideshow end -->
</div>
<!-- banner end -->