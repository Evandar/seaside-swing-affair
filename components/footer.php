<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
<!-- ================ -->
<footer id="footer" class="clearfix dark">

    <!-- .footer start -->
    <!-- ================ -->
    <div id="contact" class="footer">
        <div class="container">
            <div class="footer-inner">
                <div class="row">
                    <div class="col-lg-6 offset-lg-3">
                        <div class="footer-content text-center padding-ver-clear">
                            <h2 class="mt-4 text-center"><?php pll_e('Contact') ?></h2>
                            <div class="separator"></div>
                            <div class="logo-footer">
                                <a href="http://swingrevolution.pl">
                                    <img id="logo-footer" class="mx-auto" width="300" src="<?php echo get_template_directory_uri(); ?>/images/logo_srt.png" alt="">
                                </a>
                            </div>
                            <ul class="social-links circle animated-effect-1 margin-clear">
                                <li class="facebook"><a target="_blank" href="http://www.facebook.com/swingrevolutiontrojmiasto"><i class="fa fa-facebook"></i></a></li>
                                <li class="googleplus"><a target="_blank" href="mailto:zaloga@swingrevolution.pl"><i class="fa fa-envelope"></i></a></li>
                            </ul>
                            <div class="separator"></div>
                            <p class="text-center margin-clear">
                                Copyright © 2018 Seaside Swing Affair<br>
                                Made with love by Kasia Bąk & Tomasz Miotk
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .footer end -->
</footer>
<!-- footer end -->