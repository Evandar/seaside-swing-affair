<!-- program start -->
<!-- ================ -->
<section style="background-color: #373737;"id="program">

    <div class="full-width-section dark-bg container">
        <div class="row">
            <!-- main start -->
            <!-- ================ -->
            <div class="main col-md-12">
                <!-- page-title start -->
                <!-- ================ -->
                <h2 class="mt-4 text-center"><?php pll_e('Program'); ?></h2>
                <div class="separator"></div>
                <!-- page-title end -->
            </div>
            <!-- main end -->

        </div>
    </div>

    <!-- Fullwidth sections start -->
    <!-- ============================================================================== -->
    <!-- section start -->
    <!-- ================ -->
    <section class="full-width-section">
        <img class="to-right-block" src="<?php echo get_template_directory_uri(); ?>/images/thursday.jpg" height="734" alt="">
        <div class="full-text-container dark-bg border-bottom-clear">
            <h2 class="mt-4"><?php pll_e('Thursday'); ?></h2>
            <table class="program-table">
                <tr>
                    <td class="arrow"><i class="fa fa-chevron-right"></i></td>
                    <td>
                        <span class="title"><?php pll_e('Screening of the swing movie "Everything remains raw"'); ?></span>
                        <span class="hour">17:30</span>
                        <em><?php pll_e('After movie there will be time for little talk.') ?></em><br>
                        <em><?php pll_e('Snaks and house slippers would be welcome.') ?></em>
                        <span class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> <a href="loc_akademia">Akademia Artystyczna, ul. Aleja Grunwaldzka 339, Gdańsk</a></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="separator-2 hidden-lg-down"></div>
                    </td>
                </tr>
                <tr>
                    <td class="arrow"><i class="fa fa-chevron-right"></i></td>
                    <td>
                        <span class="title"><?php pll_e('Swing Party'); ?></span>
                        <span class="hour">20:30 - 21:30 Taster</span>
                        <span class="hour">21:30 - 02:00
                            <a href="<?php echo get_permalink(pll_get_post(get_page_by_title('Thursday Party')->ID)); ?>">
                                <?php pll_e('Swing Party'); ?>
                            </a>
                        </span>
                        <span class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> <a href="loc_zak">ŻAK, Aleja Grunwaldzka 195/197, Gdańsk</a></span>
                    </td>
                </tr>
            </table>
            <div class="separator-2 hidden-lg-down"></div>
        </div>
    </section>
    <!-- section end -->

    <!-- section start -->
    <!-- ================ -->
    <section class="full-width-section">
        <div class="full-text-container left dark-bg border-bottom-clear text-right">
            <h2 class="mt-4"><?php pll_e('Friday'); ?></h2>
            <div class="separator-3 hidden-lg-down"></div>
            <table class="program-table">
                <tr class="full-pass">
                    <td><?php pll_e('Full Pass only'); ?></td>
                </tr>
                <tr>
                    <td>
                        <span class="title"><?php pll_e('Swing Olympic Games'); ?></span>
                        <span class="hour">13:00</span>
                        <span class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> <a href="loc_park">Park Oliwski</a></span>
                    </td>
                    <td class="arrow"><i class="fa fa-chevron-left"></i></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="separator-3 hidden-lg-down"></div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span class="title"><?php pll_e('Swing Party with live music: The Shoekillers Band'); ?></span>
                        <span class="hour">21:00 - 22:00 Taster: Agnieszka & Konrad</span>
                        <span class="hour">22:00 - 05:00 Swing Party</span>
                        <span class="location"><a href="loc_gedanus">Gedanus, ul. św. Barbary 3, Gdańsk</a> <i class="fa fa-map-marker" aria-hidden="true"></i></span>
                    </td>
                    <td><i class="fa fa-chevron-left"></i></td>
                </tr>
            </table>
            <div class="separator-3 hidden-lg-down"></div>
        </div>
        <img src="<?php echo get_template_directory_uri(); ?>/images/friday.jpg" height="734" alt="">
    </section>
    <!-- section end -->
    <!-- section start -->
    <!-- ================ -->
    <section class="full-width-section">
        <img class="to-right-block" src="<?php echo get_template_directory_uri(); ?>/images/saturday.jpg" alt="" height="734">
        <div class="full-text-container dark-bg border-bottom-clear">
            <h2 class="mt-4"><?php pll_e('Saturday'); ?></h2>
            <div class="separator-2 hidden-lg-down"></div>
            <table class="program-table">
                <tr class="full-pass">
                    <td></td><td><?php pll_e('Full Pass only'); ?></td>
                </tr>
                <tr>
                    <td class="arrow"><i class="fa fa-chevron-right"></i></td>
                    <td>
                        <span class="title"><?php pll_e('Tea Dance - afternoon party in Gedanus'); ?></span>
                        <span class="hour">13:00 - 17:00</span>
                        <span class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> <a href="loc_gedanus">Gedanus, ul. św. Barbary 3, Gdańsk</a></span>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="separator-2 hidden-lg-down"></div>
                    </td>
                </tr>
                <tr>
                    <td class="arrow"><i class="fa fa-chevron-right"></i></td>
                    <td>
                        <span class="title"><?php pll_e('Birthday Party with live music: Lazy Swingers Band'); ?></span>
                        <span class="hour">21:00 - 22:00 Taster with Maria i Rafał Ślęczka</span>
                        <span class="hour">22:00 - 05:00 Swing Party</span>
                        <span class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> <a href="loc_fil_balitc">Filharmonia Bałtycka, ul. Ołowianka 1, Gdańsk</a></span>
                    </td>
                </tr>
            </table>
            <div class="separator-2 hidden-lg-down"></div>
        </div>
    </section>
    <!-- section end -->
    <!-- section start -->
    <!-- ================ -->
    <section class="full-width-section">
        <div class="full-text-container left dark-bg border-bottom-clear text-right">
            <h2 class="mt-4"><?php pll_e('Sunday'); ?></h2>
            <div class="separator-3 hidden-lg-down"></div>
            <table class="program-table">
                <tr class="full-pass">
                    <td><?php pll_e('Full Pass only'); ?></td>
                </tr>
                <tr>
                    <td>
                        <span class="title">
                            <?php pll_e('Taster: air steps on the beach')?><br>
                            <?php pll_e('with Kasia Choma & Robert Torbiczuk'); ?>
                        </span>
                        <span class="hour">14:00 - 15:00</span>
                        <em><?php pll_e('Please come in couples - no partner-rotations!') ?></em>
                        <span class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> <a href="loc_chatka_morsow"><?php pll_e('Beach near Chatka Morsów Gdyńskich'); ?></a></span>
                    </td>
                    <td class="arrow"><i class="fa fa-chevron-left"></i></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="separator-3 hidden-lg-down"></div>
                    </td>
                </tr>
                <tr class="full-pass">
                    <td><?php pll_e('Full Pass only'); ?></td>
                </tr>
                <tr>
                    <td>
                        <span class="title">
                            <?php pll_e('Ice swimming in the Baltic Sea')?><br>
                            <?php pll_e('with Irek Żak'); ?>
                        </span>
                        <span class="hour">15:00 - 15:30</span>
<!--                        <em>--><?php //pll_e('Please come in couples - no partner-rotations!') ?><!--</em>-->
                        <span class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> <a href="loc_chatka_morsow"><?php pll_e('Beach near Chatka Morsów Gdyńskich'); ?></a></span>
                    </td>
                    <td class="arrow"><i class="fa fa-chevron-left"></i></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="separator-3 hidden-lg-down"></div>
                    </td>
                </tr>
                <tr class="full-pass">
                    <td><?php pll_e('Full Pass only'); ?></td>
                </tr>
                <tr>
                    <td>
                        <span class="title"><?php pll_e('Bonfire'); ?></span>
                        <span class="hour">15:30 - 17:00</span>
<!--                        <em>--><?php //pll_e('Please come in couples - no partner-rotations!') ?><!--</em>-->
                        <span class="location"><i class="fa fa-map-marker" aria-hidden="true"></i> <a href="loc_polanka">Polanka Redłowska</a></span>
                    </td>
                    <td class="arrow"><i class="fa fa-chevron-left"></i></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="separator-3 hidden-lg-down"></div>
                    </td>
                </tr>

                <tr>
                    <td>
                        <span class="title"><?php pll_e('Swing Party'); ?></span>
                        <span class="hour">21:00 - 02:00</span>
                        <span class="location"><a href="loc_gedanus">Gedanus, ul. św. Barbary 3, Gdańsk</a> <i class="fa fa-map-marker" aria-hidden="true"></i></span>
                    </td>
                    <td><i class="fa fa-chevron-left"></i></td>
                </tr>
            </table>
            <div class="separator-3 hidden-lg-down"></div>
        </div>
        <img width="100%" src="<?php echo get_template_directory_uri(); ?>/images/sunday.jpg" alt="" height="734">
    </section>
    <!-- section end -->
</section>
    <!-- section end -->