<!-- header-container start -->
<div class="header-container">
    <!-- header start -->
    <header class="header dark fixed fixed-desktop clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-auto hidden-md-down pl-3">
                    <!-- header-first start -->
                    <!-- ================ -->
                    <div class="header-first clearfix">

                        <!-- name-and-slogan -->
                        <div class="site-slogan site-slogan-speakeasy">
                            Seaside Swing Affair
                        </div>

                    </div>
                    <!-- header-first end -->

                </div>
                <div class="col-lg-8 col-xl-9">

                    <!-- header-second start -->
                    <!-- ================ -->
                    <div class="header-second clearfix">

                        <!-- main-navigation start -->
                        <!-- ================ -->
                        <div class="main-navigation  animated">
                            <nav class="navbar navbar-toggleable-md navbar-light p-0">

                                <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar-collapse-1" aria-controls="navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                </button>
                                <div class="navbar-brand clearfix hidden-lg-up">

                                    <!-- logo -->
                                    <!-- name-and-slogan -->
                                    <div class="site-slogan site-slogan-speakeasy">
                                        Seaside Swing Affair
                                    </div>

                                </div>

                                <div class="collapse navbar-collapse" id="navbar-collapse-1">
                                    <?php
                                    wp_nav_menu( array(
                                        'menu'              => 'Navbar menu',
                                        'menu_class'        => "navbar-nav ml-xl-auto",
                                        'walker'            => new Microdot_Walker_Nav_Menu(),
                                        'container'         => false,
                                    ));
                                    ?>
                                 </div>
                            </nav>
                        </div>
                        <!-- main-navigation end -->
                    </div>
                    <!-- header-second end -->
                </div>
            </div>
        </div>
    </header>
    <!-- header end -->
</div>
<!-- header-container end -->