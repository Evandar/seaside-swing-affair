<!-- Price start -->
<!-- ================ -->
<section id="tickets" class="section dark-translucent-bg tickets-bg background-img-1">
    <div class="container">
        <div class="row grid-space-10">

            <!-- main start -->
            <!-- ================ -->
            <div class="main col-12">

                <!-- page-title start -->
                <!-- ================ -->
                <h2 class="mt-4 text-center"><?php pll_e('Tickets'); ?></h2>
                <div class="separator"></div>
                <!-- page-title end -->

                <!-- pricing tables start -->
                <!-- ================ -->
                <div class="pricing-tables object-non-visible" data-animation-effect="fadeInUpSmall"  data-effect-delay="0">
                    <div class="row">
                        <!-- pricing table start -->
                        <!-- ================ -->
                        <div class="col-md-4 plan">
                            <div class="header dark-bg">
                                <h3><?php pll_e('Party Pass'); ?></h3>
                                <div class="price"><span>120 zł</span></div>
                            </div>
                            <ul class="shadow light-gray-bg bordered">
                                <li>
                                    <?php pll_e('4 parties'); ?>
                                    <br>
                                    <?php pll_e('(from Thursday to Sunday)'); ?>
                                </li>
                                <li><?php pll_e('2 tasters'); ?></li>
                            </ul>
                        </div>
                        <!-- pricing table end -->

                        <!-- pricing table start -->
                        <!-- ================ -->
                        <div class="col-md-4 plan best-value">
                            <div class="header default-bg">
                                <h3><?php pll_e('Full Pass'); ?></h3>
                                <div class="price">
                                    180 zł
                                    <h5><?php pll_e('First 20 registrations'); ?></h5>
                                </div>
                                <div class="price">
                                    <span>200 zł</span><br>
                                </div>
                            </div>
                            <ul class="shadow light-gray-bg bordered">
                                <li>
                                    <?php pll_e('4 parties'); ?>
                                    <br>
                                    <?php pll_e('(from Thursday to Sunday)'); ?>
                                </li>
                                <li><?php pll_e('3 tasters'); ?></li>
                                <li>
                                    <?php pll_e('Tea Dance'); ?>
                                    <br>
                                    <?php pll_e('(afternoon party on Saturday)'); ?>
                                </li>
                                <li>
                                    <?php pll_e('Swing Olympic Games'); ?>
                                </li>
                                <li>
                                    <?php pll_e('Ice swimming and bonfire'); ?>
                                </li>
                            </ul>
                        </div>
                        <!-- pricing table end -->

                        <!-- pricing table start -->
                        <!-- ================ -->
                        <div class="col-md-4 plan">
                            <div class="header dark-bg">
                                <h3><?php pll_e('Birthday Party'); ?></h3>
                                <div class="price"><span>70 zł</span></div>
                            </div>
                            <ul class="shadow light-gray-bg bordered">
                                <li><?php pll_e('Saturday party with live music'); ?></li>
                            </ul>
                        </div>
                        <!-- pricing table end -->
                    </div>
                </div>
                <!-- pricing tables end -->
            </div>
            <!-- main end -->
        </div>
        <div class="col-lg-4 offset-lg-4 text-center">
            <?php
            $deadline = strtotime("2018-03-21 21:00:00");
            $now = time();
            $show = ($deadline - $now);
            ?>
            <?php if ($show <= 0): ?>
                <button href="https://docs.google.com/forms/d/e/1FAIpQLSc2wckUK_BNdyEbwWK72SLRk4-3KOrZI2qbPsyieiwqYDt9pg/viewform"
                    class="btn btn-default btn-xl"
                    disabled
                ><?php pll_e('Get tickets'); ?></button>
            <?php endif ?>
            <br>
            <a href="<?php echo get_permalink(pll_get_post(get_page_by_title('Regulations')->ID)); ?>"><?php pll_e('Event regulations');?></a>
        </div>
    </div>
</section>
<!-- section end -->